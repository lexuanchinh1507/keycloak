package com.example.keycloak.controller;

import com.example.keycloak.model.User;
import com.example.keycloak.repository.UserRepository;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.AccessToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("users")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @PostMapping
    public Boolean createUser(@RequestBody User user, @RequestHeader(name="Authorization") String token) {

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", token);
        Map<String, Object> map= new HashMap<>();
        Map<String, Object> attributes = new HashMap<>();
        attributes.put("fullName", user.getFullName());
        List<Map<String, Object>> credentials = new ArrayList<>();
        Map<String, Object> credential = new HashMap<>();
        credential.put("type", "password");
        credential.put("value", user.getPassword());
        credential.put("temporary", false);
        credentials.add(credential);

        map.put("username", user.getUsername());
        map.put("enabled", true);
        map.put("attributes", attributes);
        map.put("credentials", credentials);

        HttpEntity<Map<String, Object>> request = new HttpEntity<>(map, headers);
        String url = "http://localhost:8888/auth/admin/realms/application/users";
        try {
            ResponseEntity<String> response = restTemplate.postForEntity( url, request , String.class );
            if (response.getStatusCode() == HttpStatus.CREATED) {
                userRepository.save(user);
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @GetMapping
    public User getUserInfo(Principal principal) {
        KeycloakAuthenticationToken keycloakAuthenticationToken = (KeycloakAuthenticationToken) principal;
        AccessToken accessToken = keycloakAuthenticationToken.getAccount().getKeycloakSecurityContext().getToken();
        return userRepository.findByUsername(accessToken.getPreferredUsername());
    }
}
