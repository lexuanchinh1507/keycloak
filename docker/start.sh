#Login
echo "starting keycloak and mysql server"
docker-compose up -d

sleep 30
baseUrl="http://localhost:8888/auth"
clientId="application"
echo "init data"
token=$(
  curl -X POST "${baseUrl}/realms/master/protocol/openid-connect/token" \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -d 'username=admin&password=1&grant_type=password&client_id=admin-cli' | jq -r '.access_token' \
)

#create realm
echo "creating a new realm"
curl -X POST "${baseUrl}/admin/realms" \
 -H "Content-Type: application/json" \
 -H "Authorization: Bearer ${token}" \
 -d '{
    "realm": "application",
    "enabled": true
}'

#create client
echo "creating a new client"
curl -X POST "${baseUrl}/admin/realms/application/clients" \
 -H "Content-Type: application/json" \
 -H "Authorization: Bearer ${token}" \
 -d '{
  "clientId": "application",
  "directAccessGrantsEnabled": true,
  "protocolMappers": [
    {
      "protocol": "openid-connect",
      "config": {
        "id.token.claim": "true",
        "access.token.claim": "true",
        "userinfo.token.claim": "true",
        "multivalued": "",
        "aggregate.attrs": "",
        "user.attribute": "fullName",
        "claim.name": "fullName",
        "jsonType.label": "String"
      },
      "name": "fullName",
      "protocolMapper": "oidc-usermodel-attribute-mapper"
    }
  ]
}'

#create user
echo "creating a admin user"
curl -X POST "${baseUrl}/admin/realms/${clientId}/users" \
 -H "Content-Type: application/json" \
 -H "Authorization: Bearer ${token}" \
 -d '{
    "username": "admin",
    "enabled": true,
    "attributes" : {
        "fullName": "fullName"
    },
    "credentials": [
     {
       "type": "password",
       "value": "1",
       "temporary": false
     }
   ]
}'
