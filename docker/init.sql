USE keycloak;

create table if not exists users (
    id bigint not null auto_increment,
    username varchar(100) not null,
    password varchar(255) not null,
    full_name varchar (100),
    primary key(id),
    unique(username)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO users (username, password, full_name)
VALUES ('admin', '1', 'Lê Xuân Chính');
